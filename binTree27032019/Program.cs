﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Helper;

namespace binTree27032019
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        public static void PrintBinTree(BinTreeNode<int> t)
        {
            if(t != null)
            {
                PrintBinTree(t.GetLeft());
                Console.WriteLine(t.GetValue());
                PrintBinTree(t.GetRight());
            }
        }
    }
}
