﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BaseShape_28022019
{
     class Circle: BaseShape
    {
        private int radius;
        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;
        }
        public override double CalcArea()
        {
            return Math.PI * Math.Pow(this.radius, 2);
        }
        public override double CalcPerimeter()
        {
            return 2 * Math.PI * this.radius;
        }
        public override void Print()
        {
            base.Print();
            Console.WriteLine("Radius: {0}", this.radius);
        }
    }
}
