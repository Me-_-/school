﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseShape_28022019
{
    class Rectangle: BaseShape
    {
        private int height;
        private int width;
        public Rectangle(int x, int y, int height, int width):base(x,y)
        {
            this.height = height;
            this.width = width;
        }
        public override double CalcArea()
        {
            return this.height * this.width;
        }
        public override double CalcPerimeter()
        {
            return this.height + this.width;
        }
        public override void Print()
        {
            base.Print();
            Console.WriteLine("Height= {0}, Width= {1}", this.height, this.width);
        }
    }
}
