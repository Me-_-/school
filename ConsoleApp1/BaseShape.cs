﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BaseShape_28022019
{
    class BaseShape
    {
        private int x;
        private int y;
        private static int cnt = 0;
        public BaseShape(int x, int y)
        {
            this.x = x;
            this.y = y;
            BaseShape.cnt++;
        }
        public virtual void Print()
        {
            Console.WriteLine("Shape position: x={0}, y={1}", this.x, this.y);
            Console.WriteLine("Area: {0}", CalcArea());
            Console.WriteLine("Permeter: {0}", CalcPerimeter());
        }
        public virtual double CalcArea()
        {
            return 0;
        }
        public virtual double CalcPerimeter()
        {
            return 0;
        }
    }
}
