﻿using System;

package BinTreeNode;
public class BinTreeNode<T>
{
    private T info;
    private BinTreeNode<T> left;
    private BinTreeNode<T> right;
    public BinTreeNode(T info)
    {
        this.info = info;
        this.left = null;
        this.right = null;
    }
    public BinTreeNode(BinTreeNode<T> left, T info, BinTreeNode<T> right)
    {
        this.info = info;
        this.left = left;
        this.right = right;
    }
    public T GetInfo()
    {
        return info;
    }
    public BinTreeNode<T> GetLeft()
    {
        return left;
    }
    public BinTreeNode<T> GetRight()
    {
        return right;
    }
    public void SetInfo(T info)
    {
        this.info = info;
    }
    public void SetLeft(BinTreeNode<T> left)
    {
        this.left = left;
    }
    public void SetRight(BinTreeNode<T> right)
    {
        this.right = right;
    }
    public String ToString()
    {
        return " ( " + left + " " + info + " " + right + " ) ";
    }
}
package List;
public class List<T>
{
    private Node<T> first;
    /* הפעולה בונה ומחזירה רשימה ריקה */
    public List()
    {
        this.first = null;
    }
    /*הפעולה מחזירה את החוליה הראשונה ברשימה 
      אם הרשימה ריקה הפעולה מחזירה null*/
    public Node<T> GetFirst()
    {
        return first;
    }
    /*הפעולה מכניסה לרשימה חוליה חדשה שהערך שלה הוא info  אחרי החוליה p 
    הפעולה מחזירה את החוליה החדשה שהוכנסה
    כדי להכניס איבר ראשון לרשימה הערך של הפרמטר p צריך להיות null
    הנחה: החוליה next קיימת ברשימה   */
    public Node<T> Insert(Node<T> p, T info)
    {
        Node<T> q = new Node<T>(info);
        if (p == null)
        {
            q.SetNext(first);
            first = q;
        }
        else
        {
            q.SetNext(p.GetNext());
            p.SetNext(q);
        }
        return q;
    }
    /*הפעולה מוציאה את החוליה p מן הרשימה ומחזירה את החוליה הבאה אחריה
    אם הוצאה החוליה האחרונה ברשימה הפעולה תחזיר null
    הנחה: החוליה p קיימת ברשימה*/
    public Node<T> Remove(Node<T> p)
    {
        if (first == p)
        {
            first = p.GetNext();
            return first;
        }
        else
        {
            Node<T> prev = first;
            while (prev.GetNext() != p)
                prev = prev.GetNext();
            prev.SetNext(p.GetNext());
            return prev.GetNext();
        }
    }
    /* הפעולה מחזירה 'אמת' אם הרשימה ריקה, ומחזירה 'שקר' אחרת **/
    public boolean IsEmpty()
    {
        return first == null;
    }
    /* הפעולה מחזירה מחרוזת המתארת את הרשימה */
    public String ToString()
    {
        String s = "[";
        Node<T> p = this.first;
        while (p != null)
        {
            s = s + p.GetInfo().ToString();
            if (p.GetNext() != null)
                s = s + ",";
            p = p.GetNext();
        }
        s = s + "]";
        return s;
    }
}
package Node;
public class Node<T>
{
    private T info;
    private Node<T> next;
    /* הפעולה בונה ומחזירה חוליה שהערך שלה הוא info ואין לה חוליה עוקבת **/
    public Node(T info)
    {
        this.info = info;
        this.next = null;
    }
    /*הפעולה בונה ומחזירה חוליה, שהערך שלה הוא info
      והחוליה העוקבת לה היא החוליה next */
    public Node(T info, Node<T> next)
    {
        this.info = info;
        this.next = next;
    }
    /* הפעולה מחזירה את הערך של החוליה הנוכחית **/
    public T GetInfo()
    {
        return info;
    }
    /* הפעולה מחזירה את החוליה העוקבת לחוליה הנוכחית **/
    public Node<T> GetNext()
    {
        return next;
    }
    /* הפעולה קובעת את ערך החוליה הנוכחית להיות  info **/
    public void SetInfo(T info)
    {
        this.info = info;
    }
    /* הפעולה קובעת את החוליה העוקבת לחוליה הנוכחית להיות החוליה next **/
    public void SetNext(Node<T> next)
    {
        this.next = next;
    }
    /* הפעולה מחזירה מחרוזת המתארת את החוליה הנוכחית */


    public String ToString()
    {
        return this.info.toString();
    }
}//Class
package Queue;
public class Queue<T>
{
    private Node<T> first;
    private Node<T> last;


    /* הפעולה בונה ומחזירה תור ריק **/
    public Queue()
    {
        this.first = null;
        this.last = null;
    }
    /* הפעולה מכניסה את הערך x לסוף התור הנוכחי **/
    public void Insert(T x)
    {
        Node<T> temp = new Node<T>(x);
        if (first == null)
            first = temp;
        else
            last.SetNext(temp);
        last = temp;
    }
    /* הפעולה מוציאה ומחזירה את הערך הנמצא  בראש התור הנוכחי **/
    public T Remove()
    {
        T x = first.GetInfo();
        first = first.GetNext();
        if (first == null)
            last = null;
        return x;
    }
    /* הפעולה מחזירה את הערך הנמצא  בראש התור הנוכחי **/
    public T Head()
    {
        return first.GetInfo();
    }
    /* הפעולה מחזירה אמת אם התור הנוכחי ריק או שקר אחרת **/
    public boolean IsEmpty()
    {
        return first == null;
    }
    /* הפעולה מחזירה מחרוזת המתארת את התור הנוכחי */
    public String ToString()
    {
        String s = "[";
        Node<T> p = this.first;
        while (p != null)
        {
            s = s + p.GetInfo().ToString();
            if (p.GetNext() != null)
                s = s + ",";
            p = p.GetNext();
        }
        s = s + "]";
        return s;
    }


}
package Stack;
public class Stack<T>
{
    private Node<T> head;
    /* הפעולה בונה ומחזירה מחסנית ריקה **/
    public Stack()
    {
        this.head = null;
    }
    public void Push(T x)
    {
        Node<T> temp = new Node<T>(x);
        temp.SetNext(head);
        head = temp;
    }
    /* הפעולה מכניסה את הערך x לראש המחסנית הנוכחית **/
    /* הפעולה מוציאה ומחזירה את הערך הנמצא  בראש המחסנית הנוכחית **/
    public T Pop()
    {
        T x = head.GetInfo();
        head = head.GetNext();
        return x;
    }


    /* הפעולה מחזירה את הערך הנמצא  בראש המחסנית הנוכחית **/
    public T Top()
    {
        return head.GetInfo();
    }


    /* הפעולה מחזירה 'אמת' אם המחסנית הנוכחית ריקה, ומחזירה 'שקר' אחרת **/
    public boolean IsEmpty()
    {
        return head == null;
    }


    /* הפעולה מחזירה מחרוזת המתארת את המחסנית הנוכחית */
    public String ToString()
    {
        String s = "[";
        Node<T> p = this.head;
        while (p != null)
        {
            s = s + p.GetInfo().ToString();
            if (p.GetNext() != null)
                s = s + ",";
            p = p.GetNext();
        }
        s = s + "]";
        return s;
    }


}